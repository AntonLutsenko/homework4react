import React, { useEffect, useState } from "react";
import Header from "../Header/Header";
import { Routes, Route } from "react-router-dom";
import ProductList from "../ProductList/ProductList";
import Basket from "../Basket/Basket";
import { useSelector, useDispatch } from "react-redux";
import modalParams from "../Modal/modalParams";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import Favorites from "../Favorites/Favorites";
import { getItems, setItems } from "../../store/actions/itemsActions";
import { recivedIdGoodsInFavorite } from "../../store/actions/favoriyeActions";
import { recivedIdGoodsInCard } from "../../store/actions/cardsActions";
import {
	closedModalWin,
	openModalWin,
	recivedPropertiesModalWin,
} from "../../store/actions/modalActions";

const MarcetPlace = () => {
	const state = useSelector((state) => state);
	const modalDeclaration = useSelector((state) => state.modal.modalDeclaration);
	const cards = useSelector((state) => state.cards.cards);
	const targetProd = useSelector((state) => state.modal.targetProd);
	const items = useSelector((state) => state.items.items);

	const dispatch = useDispatch();
	console.log();

	useEffect(() => {
		(async () => {
			dispatch(setItems(await getItems()));
		})();
		if (!localStorage.getItem("card")) {
			localStorage.setItem("card", "[]");
		}

		if (!localStorage.getItem("favorites")) {
			localStorage.setItem("favorites", "[]");
		}
	}, []);

	useEffect(() => {
		const newFavorite = JSON.parse(localStorage.getItem("favorites"));
		dispatch(recivedIdGoodsInFavorite(newFavorite));
	}, []);

	useEffect(() => {
		const cardUpdate = JSON.parse(localStorage.getItem("card"));
		dispatch(recivedIdGoodsInCard(cardUpdate));
	}, []);

	const onClickApplyBtn = (e) => {
		let newCards = cards.map((elem) => elem);
		if (e.target.dataset.modalId === "modalID2") {
			console.log(targetProd);
			newCards.push(targetProd);
			localStorage.setItem("card", JSON.stringify(newCards));
		}
		if (e.target.dataset.modalId === "modalID3") {
			newCards = cards.map((elem) => elem);
			if (newCards.includes(targetProd)) {
				newCards = newCards.filter((item) => item !== targetProd);
				localStorage.setItem("card", JSON.stringify(newCards));
			}
		}
		dispatch(closedModalWin());
		dispatch(recivedIdGoodsInCard(newCards));
	};

	useEffect(() => {
		const [modalWindow] = Object.values(modalParams);
		dispatch(recivedPropertiesModalWin(modalWindow));
	}, []);
	const didMount = () => {
		dispatch(openModalWin());
	};

	const didUnmounted = () => {
		dispatch(closedModalWin());
	};
	const filterBy = (a, b) => {
		let typedArr = a.filter(function (a) {
			return a.id === b.find((item) => item === a.id);
		});
		return typedArr;
	};

	return (
		<>
			<Header />
			<Routes>
				<Route path="/" element={<ProductList items={items} />}></Route>
				<Route
					path="basket"
					element={<Basket filter={filterBy} items={items} />}
				></Route>
				<Route
					path="favorites"
					element={<Favorites filter={filterBy} items={items} />}
				></Route>
			</Routes>
			{state.modal.displayState === "open" && (
				<Modal
					className={modalDeclaration.className}
					id={modalDeclaration.id}
					onClick={didUnmounted}
					show={didMount}
					hidden={didUnmounted}
					header={modalDeclaration.header}
					closeButton={modalDeclaration.closeButton}
					description={modalDeclaration.description}
					actions={
						<>
							<Button
								id={modalDeclaration.id}
								className={modalDeclaration.classNameButton}
								dataModal={modalDeclaration.id}
								text={modalDeclaration.textBtnLeft}
								onClick={onClickApplyBtn}
							/>
							<Button
								className={modalDeclaration.classNameButton}
								text={modalDeclaration.textBtnRight}
								onClick={didUnmounted}
							/>
						</>
					}
				/>
			)}
		</>
	);
};

export default MarcetPlace;
