const modalParams = [
	{
		id: "modalID1",
		className: "modal modal_info",
		header: "Добавить данный продукт в избранные?",
		description: `Данный телефон будет добавлен в избранные`,
		backgroundColor: "rgb(15 193 74 / 90%)",
		closeButton: false,
		textBtnLeft: "Ok",
		textBtnRight: "Cancel",
		classNameButton: "btn modal_btn",
	},
	{
		id: "modalID2",
		className: "modal modal_info",
		header: "Добавить данный продукт в корзину?",
		description: `Данный телефон будет добавлен в корзину покупок`,

		backgroundColor: "rgb(15 193 74 / 90%)",
		closeButton: false,
		textBtnLeft: "Ok",
		textBtnRight: "Cancel",
		classNameButton: "btn modal_btn",
	},
	{
		id: "modalID3",
		className: "modal modal__add-to-card",
		header: "Удалить данный продукт из корзины?",
		description: `Данный телефон будет удален из корзины`,
		backgroundColor: "rgb(15 193 74 / 90%)",
		closeButton: false,
		textBtnLeft: "Delete",
		textBtnRight: "Cancel",
		classNameButton: "btn modal_btn",
	},
];

export default { modalParams };
