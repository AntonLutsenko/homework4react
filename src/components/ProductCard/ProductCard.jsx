import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { recivedIdGoodsInFavorite } from "../../store/actions/favoriyeActions";
import {
	openModalWin,
	recivedPropActiveModal,
	selectIdForActions,
} from "../../store/actions/modalActions";
import Button from "../Button/Button";
import "./productCard.scss";

const ProductCard = ({
	id,
	className,
	isFavorite,
	routeImg,
	name,
	price,
	color,
	inCard,
}) => {
	const favorites = useSelector((state) => state.favorites.favorites);

	const modalWindow = useSelector((state) => state.modal.modalWindow);
	const dispatch = useDispatch();

	const addToFavorites = (e) => {
		const targetId = e.target.parentElement.id;
		let newProducts = favorites.map((targetId) => targetId);
		if (newProducts.includes(targetId)) {
			newProducts = newProducts.filter((item) => item !== targetId);
			localStorage.setItem("favorites", JSON.stringify(newProducts));
		} else {
			newProducts.push(targetId);
			localStorage.setItem("favorites", JSON.stringify(newProducts));
		}
		dispatch(recivedIdGoodsInFavorite(newProducts));
	};

	const openModal = (e) => {
		const modalID = e.target.dataset.modalId;
		let modalTarget = modalWindow.find((item) => item.id === modalID);
		dispatch(recivedPropActiveModal(modalTarget));
		dispatch(selectIdForActions(e.target.id));
		dispatch(openModalWin());
	};

	return (
		<li className="product-card" id={id} key={id}>
			<svg
				id={id}
				onClick={addToFavorites}
				className={`${className}${
					isFavorite ? "--js-active" : "--js-non-active"
				}`}
				viewBox="0 0 16 17"
				width="19"
				height="21"
				fill="none"
				xmlns="http://www.w3.org/2000/svg"
			>
				<path
					d="M14.1891 6.37042L10.2219 5.79385L8.44847 2.19854C8.40003 2.1001 8.32034 2.02042 8.2219 1.97198C7.97503 1.8501 7.67503 1.95167 7.55159 2.19854L5.77815 5.79385L1.81097 6.37042C1.70159 6.38604 1.60159 6.4376 1.52503 6.51573C1.43247 6.61086 1.38146 6.73885 1.38322 6.87157C1.38498 7.0043 1.43936 7.13089 1.5344 7.22354L4.40472 10.022L3.72659 13.9735C3.71069 14.0655 3.72086 14.16 3.75595 14.2464C3.79105 14.3329 3.84966 14.4077 3.92514 14.4626C4.00062 14.5174 4.08995 14.55 4.183 14.5566C4.27605 14.5632 4.3691 14.5437 4.45159 14.5001L8.00003 12.6345L11.5485 14.5001C11.6453 14.5517 11.7578 14.5689 11.8657 14.5501C12.1375 14.5032 12.3203 14.2454 12.2735 13.9735L11.5953 10.022L14.4657 7.22354C14.5438 7.14698 14.5953 7.04698 14.611 6.9376C14.6532 6.66417 14.4625 6.41104 14.1891 6.37042Z"
					stroke="rgb(63, 62, 60)"
				/>
			</svg>

			<div className="product-card__image">
				<img src={routeImg} alt={name} style={{ width: "auto", height: 300 }} />
			</div>
			<div className="product-card__info">
				<div className="product-card__name">{name}</div>
				<div className="product-card__price">Price:{price}</div>
				<div className="product-card__description">Color:{color}</div>
				<Button
					id={id}
					className={` btn ${
						inCard ? "btn__added-to-card" : "btn__add-to-card"
					}`}
					dataModal={"modalID2"}
					onClick={inCard ? null : openModal}
					text={`${inCard ? "Added to card" : "Add to card"}`}
				/>
				{inCard && (
					<button
						id={id}
						className="btn__close"
						data-modal-id={"modalID3"}
						onClick={openModal}
					>
						X
					</button>
				)}
			</div>
		</li>
	);
};

export default ProductCard;
