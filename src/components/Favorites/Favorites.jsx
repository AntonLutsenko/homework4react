import React from "react";

import { useSelector } from "react-redux";
import ProductList from "../ProductList/ProductList";

const Favorites = ({ filter }) => {
	const items = useSelector((state) => state.items.items);
	const favorites = useSelector((state) => state.favorites.favorites);
	const favoritesList = filter(items, favorites);

	return (
		<>
			<ProductList items={favoritesList} />
		</>
	);
};

export default Favorites;
