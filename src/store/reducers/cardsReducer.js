const initialState = {
	cards: [],
};

export const cardReducer = (state = initialState, action) => {
	switch (action.type) {
		case "RECEIVED_ID_GOODS_IN_CARD": {
			return {
				...state,
				cards: action.payload,
			};
		}
		default:
			return state;
	}
};
