const initialState = {
	displayState: "none",
	modalWindow: null,
	modalDeclaration: {},
	targetProd: null,
};

export const modalReducer = (state = initialState, action) => {
	switch (action.type) {
		case "OPENED_MODAL_WINDOW": {
			return {
				...state,
				classWrapper: "wrapper",
				displayState: "open",
			};
		}
		case "CLOSED_MODAL_WINDOW": {
			return {
				...state,
				classWrapper: "",
				displayState: "none",
			};
		}
		case "RECEIVED_PROPERTIES_MODAL_WINDOWS": {
			return {
				...state,
				modalWindow: action.payload,
			};
		}
		case "RECEIVED_PROPERTIES_ACTIVE_MODAL_WINDOW": {
			return {
				...state,
				modalDeclaration: action.payload,
			};
		}
		case "SELECTED_ID_ITEM_FOR_ACTIONS": {
			return {
				...state,
				targetProd: action.payload,
			};
		}
		default:
			return state;
	}
};
