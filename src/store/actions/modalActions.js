export const openModalWin = () => {
	return {
		type: "OPENED_MODAL_WINDOW",
	};
};

export const closedModalWin = () => {
	return {
		type: "CLOSED_MODAL_WINDOW",
	};
};
export const recivedPropertiesModalWin = (modalWindow) => {
	return {
		type: "RECEIVED_PROPERTIES_MODAL_WINDOWS",
		payload: modalWindow,
	};
};

export const recivedPropActiveModal = (modalDeclaration) => {
	return {
		type: "RECEIVED_PROPERTIES_ACTIVE_MODAL_WINDOW",
		payload: modalDeclaration,
	};
};

export const selectIdForActions = (targetProd) => {
	return {
		type: "SELECTED_ID_ITEM_FOR_ACTIONS",
		payload: targetProd,
	};
};
